require 'test_helper'

class CostsControllerTest < ActionController::TestCase
  setup do
    @cost = costs(:one)
	@category = categories(:one)
  end

  test "should get index" do
    get :index, :locale => 'en'
    assert_response :success
    assert_not_nil assigns(:costs)
  end

  test "should get new" do
    get :new, :locale => 'en'
    assert_response :success
  end

  test "should create cost" do
    assert_difference('Cost.count') do
      post :create, category_id: @category.id, cost: @cost.attributes, :locale => 'en'
    end

    assert_redirected_to category_path(assigns(:category))
  end

  test "should show cost" do
    get :show, id: @cost, :locale => 'en'
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cost, :locale => 'en'
    assert_response :success
  end

  test "should update cost" do
    patch :update, id: @cost, cost: { amount: @cost.amount, category_id: @cost.category_id }, :locale => 'en'
    assert_redirected_to cost_path(assigns(:cost))
  end

  test "should destroy cost" do
    assert_difference('Cost.count', -1) do
      delete :destroy, id: @cost, :locale => 'en'
    end

    assert_redirected_to costs_path
  end
  
  test "should not create cost without amount" do
	cost = Cost.new
	assert_not cost.save, "Saved without entering amount"
  end
end
