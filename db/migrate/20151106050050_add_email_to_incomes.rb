class AddEmailToIncomes < ActiveRecord::Migration
  def change
    add_column :incomes, :email, :string
  end
end
